package agile.corso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * Hello world!
 *
 */
public class App 
{
	private static boolean isTurnedOn;
	private static Stack<String> stack;

    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }

	public static boolean pushValue(String c) {
		if(c.equals("o")) {
			stack = new Stack<String>();
			isTurnedOn = true;
			return true;
		} else if(c.equals("f")) {
			clear();
			isTurnedOn = false;
			return true;
		} else if(c.equals("=")) {
			calculate();
			return true;
		} else if (isTurnedOn()) {
			stack.push(c);
			return true;
		}

		return false;
	}

	private static void clear(){
		if (stack != null)
			stack.clear();
	}

	public static boolean isTurnedOn() {
		return isTurnedOn;
	}

	public static void setTurnedOn(boolean switchStateTo) {
		isTurnedOn = switchStateTo;
	}

	public static Stack<String> getStack() {
		return stack;
	}

	private static List<String> mathOperators = Arrays.asList("+", "-", "*", "/");

	private static void calculate(){
	    
        while (containsOperator()) {
            Integer result = null;
            int i=0;
            List<Integer> toRemove = new ArrayList<Integer>();

            for (String v : stack) {
                if (mathOperators.contains(v)) {
                    char operator = stack.get(i).charAt(0);
                    int firstNumber = Integer.parseInt(stack.get(i - 2));
                    int secondNumber = Integer.parseInt(stack.get(i - 1));
                    toRemove.add(i);
                    toRemove.add(i - 1);
                    toRemove.add(i - 2);

                    switch (operator) {
                        case '+':
                            result = firstNumber + secondNumber;
                            break;
                        case '-':
                            result = firstNumber - secondNumber;
                            break;
                        case '*':
                            result = firstNumber * secondNumber;
                            break;
                        case '/':
                            result = firstNumber / secondNumber;
                            break;
                    }
                    break;
                }
                i++;
            }

            for (int r : toRemove) {
                if (r == toRemove.get(toRemove.size() - 1) && result != null) {
                    stack.set(r, result.toString());
                } else {
                    stack.remove(r);
                }
            }
        }

	}

	private static boolean containsOperator(){
	    return stack.contains("+")
                || stack.contains("-")
                || stack.contains("*")
                || stack.contains("/");
    }
}
