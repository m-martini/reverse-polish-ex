package agile.corso;


import java.util.Stack;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    
    public void tearDown() {
    	App.setTurnedOn(false);
    }
    
    public void testWhenInsertingOCalculatorTurnOn()
    {  	 	
    	boolean isValidPush = App.pushValue("o");
        assertTrue(isValidPush);
        assertTrue(App.isTurnedOn());
    }

    public void testWhenInsertingValuesOnCalculatorOff() {
    	boolean isValidPush = App.pushValue("5");
        assertFalse(isValidPush);
        assertFalse(App.isTurnedOn());
    }

    public void testWhenInsertingValuesOnCalculatorOn()
    {
        App.pushValue("o");

        boolean isValidPush = App.pushValue("5");
        assertTrue(isValidPush);
    }

    public void testWhenInsertingFCalculatorTurnOffAndCleanStack()
    {
    	App.pushValue("o");

        boolean isValidPush = App.pushValue("f");
        assertTrue(isValidPush);
        assertFalse(App.isTurnedOn());
        assertTrue(App.getStack().empty());
    }

    public void testWhenInsertingFAndCalculatorIsOffIgnore()
    {
        boolean isValidPush = App.pushValue("f");
        assertTrue(isValidPush);
        assertFalse(App.isTurnedOn());
    }

    public void testWhenInsertingOAndCalculatorIsOnIgnore()
    {
        App.pushValue("o");

        boolean isValidPush = App.pushValue("o");
        assertTrue(isValidPush);
        assertTrue(App.isTurnedOn());
    }

    public void testWhenInsertingValueAndCalculatorIsOnStackRegisterValue()
    {
        App.pushValue("o");

        boolean isValidPush = App.pushValue("2");
        assertTrue(isValidPush);

        Stack<String> testStack = new Stack<String>();
        testStack.push("2");

        assertEquals(testStack, App.getStack());
    }

    public void testWhenInsertingResultTokenAndCalculatorIsOnReturnValues()
    {
        App.pushValue("o");

        boolean isValidPush = App.pushValue("2");
        assertTrue(isValidPush);

        Stack<String> testStack = new Stack<String>();
        testStack.push("2");

        assertEquals(testStack, App.getStack());

        isValidPush = App.pushValue("=");
        assertTrue(isValidPush);

        assertEquals(testStack, App.getStack());
    }


    public void testWhenInsertingResultTokenAndStackIsEmptyReturnEmptyStack()
    {
        App.pushValue("o");

        boolean isValidPush = App.pushValue("=");
        assertTrue(isValidPush);

        Stack<String> testStack = new Stack<String>();
        assertEquals(testStack, App.getStack());
    }

    public void test()
    {
        Integer firstValue = 7;
        Integer secondValue = 5;

        App.pushValue("o");
        App.pushValue(firstValue.toString());
        App.pushValue(secondValue.toString());
        App.pushValue("*");
        App.pushValue(secondValue.toString());
        App.pushValue("+");

        App.pushValue("=");

        Stack<String> testStack = new Stack<String>();
        testStack.push((firstValue * secondValue + secondValue) + "");

        assertEquals(testStack, App.getStack());
    }

    
}
