# README #

### Reverse Polish Notation Calculator ###
General specifications: https://en.wikipedia.org/wiki/Reverse_Polish_notation
Additional specifications for this project:
• Implement the solution as a library that can be reused by multiple client programs.
• Handle illogical states in the calculator gracefully (for example, using the first algorithm
from the Wikipedia article, if the stack is empty, then entering an operator would cause
a pop, which makes no sense as the stack is empty).
Operators are:
+ addition
- subtraction
* multiplication
/ division
e exponentiation
r root
To get the result:
= this symbol is called the result token in the following stories
Additional tokens:
o turn on (make the calculator available)*
f turn off (make the calculator unavailable)*
c clear (clear the calculator’s memory)
u undo (back out the most recent input)
* For some client implementations, the concept of turning the calculator on and off may
not be meaningful. The calculator code must be able to handle it in any case.
Operand values can contain:
Decimal digits
Decimal point (period or comma)

### Story #1: Calculator can be turned on and off ###
Scenario: Calculator changes state from “off” to “on”
Given the calculator is off
When the client turns the calculator on
Then the calculator is ready to accept input
Scenario: Calculator changes state from “on” to “off”
Given the calculator is on
When the client turns the calculator off
Then the calculator clears its memory
Scenario: Calculator ignores on/off requests when in the wrong state
Given the calculator is off
When the client turns the calculator off
Then the calculator silently ignores the input
Given the calculator is on
When the client turns the calculator on
Then the calculator silently ignores the input

### Story #2: Calculator can store and return a value ###
Background:
Given the calculator has been turned on
Scenario: Calculator stores the value entered by the client
Given the calculator’s memory is clear
When the client enters a token that represents an operand
Then the calculator stores the value
Scenario: Calculator returns a result
Given the calculator contains a value
When the client enters the result token
Then the calculator returns the stored value
Given the calculator does not contain a value
When the client enters the result token
Then the calculator returns an empty result


### Story #3: Calculator can perform addition on two operands ###
Background:
Given the calculator has been turned on
And the calculator’s memory is clear
Scenario: Addition of two integers
Given the client enters the values “5”, “3”, and “+”
When the client enters the result token
Then the calculator returns “8”
Scenario: Addition of two reals
Given the client enters the values “2.5”, “4.25”, and “+”
When the client enters the result token
Then the calculator returns “6.75”
Scenario: Addition of an integer and a real
Given the client enters the values “2.5”, “3”, and “+”
When the client enters the result token
Then the calculator returns “5.5”
Given the client enters the values “3”, “6.9”, and “+”
When the client enters the result token
Then the calculator returns “9.9”
### Story #4: Calculator memory can be cleared ###
Scenario: Clearing the calculator’s memory
Given the calculator has been turned on
And the calculator’s memory is clear
And the client enters the value “1”
And the client enters the value “c”
When the client enters the result token
Then the calculator returns an empty result
Page 3 of 6
### Story #5: Calculator handles invalid input values ###
Background:
Given the calculator has been turned on
And the calculator’s memory is clear
Scenario: Handling badly-formatted request document
When the client submits an incorrectly-formatted request document
Then the calculator returns an error document
Scenario: Handling invalid resource identifier
When the client submits a request for a nonexistent resource
Then the calculator returns an error document
Scenario: Handling invalid operand values
When the client submits an invalid operand value
Then the calculator returns an error document
Scenario: Handling invalid operator values
When the client submits an undefined operator value
Then the calculator returns an error document
Scenario: Handling operand and operator values submitted in the wrong order
When the client submits input values in the wrong order
Then the calculator returns an error document

### Story #6: Calculator can perform subtraction on two operands ###
Background:
Given the calculator has been turned on
And the calculator’s memory is clear
Scenario: Subtraction of two integers
Given the client enters the values “4”, “9”, and “-”
When the client enters the result token
Then the calculator returns “-5”
Scenario: Subtraction of two reals
Given the client enters the values “4.25”, “8.5”, and “-”
When the client enters the result token
Then the calculator returns “-4.25”
Page 4 of 6
Scenario: Subtraction of an integer from a real
Given the client enters the values “3”, “12.5”, and “-”
When the client enters the result token
Then the calculator returns “-9.5”
Scenario: Subtraction of a real from an integer
Given the client enters the values “13.446”, “26”, and “-”
When the client enters the result token
Then the calculator returns “-12.554”

### Story #7: Calculator can perform multiple operations ###
Background:
Given the calculator has been turned on
And the calculator’s memory is clear
Scenario: Calculation of 15, 7, 1, 1, +, -
Given the client enters the values “15”, “7”, “1”, “1”, “+”, “-”, and “+”
When the client enters the result token
Then the calculator returns “20”
Given the client enters the values “4.5”, “8”, +, “6.5”, and “-”
When the client enters the result token
Then the calculator returns “6”

### Story #8: Calculator can perform multiplication ###
Background:
Given the calculator has been turned on
And the calculator’s memory is clear
Scenario: Multiply two values
Given the client enters the values “2”, “3”, and “*”
When the client enters the result token
Then the calculator returns “6”
Scenario: Multiply three values
Given the client enters the values “4”, “5”, “2”, “*”, and “*”
When the client enters the result token
Then the calculator returns “40”

### Story #9: Calculator can perform division ###
Background:
Given the calculator has been turned on
And the calculator’s memory is clear
Scenario: Divide one value by another
Given the client enters the values “21”, “3”, and “/”
When the client enters the result token
Then the calculator returns “7”
Scenario: Divide a value by a larger value
Given the client enters the values “4”, “5” and “/”
When the client enters the result token
Then the calculator returns “0.8”

### Story #10: Calculator can compute a complicated expression ###
Background:
Given the calculator has been turned on
And the calculator’s memory is clear
Scenario: Compute ((15 ÷ (7 − (1 + 1))) × 3) − (2 + (1 + 1))
Given the client enters the values
“15”, “7”, “1”, “1”, “+”, “-”, “/”, “3”, “*”, “2”, “1”, “1”, “+”, “+”, “-”
When the client enters the result token
Then the calculator returns “5”
